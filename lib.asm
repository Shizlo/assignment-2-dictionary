section .text
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy


%define SYS_EXIT 60
%define SYS_WRITE 1
%define SYS_READ 0

%define LINE_BREAK 0xA
%define HORIZONTAL_TAB 0x9
%define SPACE 0x20

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .next:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .next
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
    push rsi
    call string_length
    pop rdi
	pop rsi
    mov rdx, rax
    mov rax, SYS_WRITE
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, SYS_WRITE
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, LINE_BREAK
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov rsi, 10
    mov r10, rsp
    dec rsp
    mov byte [rsp], 0
    .divide:
        xor rdx, rdx
        div rsi
        add dl, '0'
        dec rsp
        mov [rsp], dl
        test rax, rax
        jnz .divide
    mov rdi, rsp
	push r10
    call print_string
	pop r10
    mov rsp, r10
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    test rdi, rdi
    jns .print
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .print:
       jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    mov al, byte [rdi]
    cmp al, byte [rsi]
    jne .error
    inc rdi
    inc rsi
    test al, al
    jnz string_equals
    mov rax, 1
    ret
    .error:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rax, SYS_READ
    xor rdi, rdi
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx, rcx
    .skip:
        push rdi
        push rsi
        push rcx
        call read_char
        pop rcx
        pop rsi
        pop rdi
        cmp al, SPACE
        je .skip
        cmp al, HORIZONTAL_TAB
        je .skip
        cmp al, LINE_BREAK
        je .skip
        test al, al
        jz .end
    .read:
        mov byte [rdi+rcx], al
        inc rcx
        push rdi
        push rsi
        push rcx
        call read_char
        pop rcx
        pop rsi
        pop rdi
        dec rsi
        cmp rcx, rsi
        jg .error
        inc rsi
        cmp al, SPACE
        je .end
        cmp al, HORIZONTAL_TAB
        je .end
        cmp al, LINE_BREAK
        je .end
        test al, al
        jz .end
        jmp .read
    .error:
        xor rax, rax
        ret
    .end:
        mov byte [rdi+rcx], 0
        mov rax, rdi
        mov rdx, rcx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov rsi, 10
    .compare:
        mov r10, 0
        mov r10b, byte [rdi+rcx]
        cmp r10b, '9'
        ja .end
        cmp r10b, '0'
        jb .end
        inc rcx
        mul rsi
        sub r10, `0`
        add rax, r10
        jmp .compare
    .end:
        mov rdx, rcx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    mov al, byte [rdi]
    cmp al, '-'
    je .negative
    jmp parse_uint
    .negative:
        inc rdi
        call parse_uint
        neg rax
        test rdx, rdx
        jz .skip
        inc rdx
    .skip:
        ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length

    cmp rax, rdx
    jnb .error

    .copy:
        mov dl, byte [rdi]
        mov byte [rsi], dl
        inc rdi
        inc rsi
        test dl, dl
        jnz .copy
        ret

    .error:
        xor rax, rax
        ret
