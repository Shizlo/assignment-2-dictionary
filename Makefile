COMPILER=nasm
COMPILE_FLAGS=-felf64 -g
LINKER=ld
TARGET=dictionary

all: main.o dict.o lib.o
	$(LINKER) $? -o $(TARGET)

%.o: %.asm
	$(COMPILER) $(COMPILE_FLAGS) $< -o $@

clean:
	rm -f *.o

.PHONY: all clean
