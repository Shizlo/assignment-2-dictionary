%include "lib.inc"

global _start
extern find_word

%define LINE_BREAK 0xA

section .rodata
	too_long_error: db "Message too long, max size is 256 symbols", LINE_BREAK, 0
	key_not_found_error: db "Such key was not found in the dictionary", LINE_BREAK, 0

section .data
	%include "words.inc"

section .bss
	buffer: resb 256

section .text

_start:
	mov rdi, buffer
	mov rsi, 256
	call read_word
	push rdx
	cmp rax, 0
	je .too_long

	mov rdi, buffer
	mov rsi, NEXT
	call find_word
	cmp rax, 0
	je .key_not_found
	pop rdx
	lea rdi, [rax + 8 + 1 + rdx]
	mov rsi, STDOUT
	call print_string
	call exit

.too_long:
	mov rdi, too_long_error
	jmp .print_error_message

.key_not_found:
	mov rdi, key_not_found_error

.print_error_message:
	mov rsi, STDERR
	call print_string
	call exit
